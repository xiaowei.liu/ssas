#include <stdio.h>
#include <string.h>
#include <unistd.h>

/* Version INFO 15.11.2021   
Author: LDae
Change: new Module/File due to Redesign
Upload/Create in GitLab: 29.11.2021

*/


typedef unsigned short int UINT16;
typedef unsigned char UINT8;

#define Halt 1
#define Left 2
#define Right 3

void automat(UINT8 x , UINT8 *y)
{
	static UINT8 z = Halt; /* Initialzustand */
	int a = 0;
	int b = 0;
	int i = 0;
	switch ( z )
	{
		case Halt :
		/*Aktionen   Deviation ID D-8-0 (Rule 9 MISRA HIS-Subset)
		// *y = 0x0 << 0); // linkslauf aus
		// *y &= ~(0x1 << 1); // rechtslauf aus
		*/
		/* Alternativ */
		*y = 0x0; /* Alle aus */
		/* Transitionen */
		if (x & (0x1 << 2)  && (x & (0x1 << 7) ) )	/* Prio 3 */
			z = Right;				
		if (x & (0x1 << 1)  && (x & (0x1 << 6) ) )	/* Prio 2 */
			z = Left;				
		if ( !(x & (0x1 << 0) ) ) 	/* Prio 1 */
			z = Halt;				
		break; 
		
		case Left :
		/* Aktionen */
		*y = (0x1 << 0); /* NUR Linkslauf setzen */
		
		/* Transitionen */
		if ( !(x & (0x1 << 0)) || !(x & (0x1 << 6)) ) 
			z = Halt;				
		break; 
		
		case Right:
		/* Aktion */
		*y = (0x1 << 1); /* NUR Rechtslauf setzen */
		
		/* Transitionen */
		if (! (x & (0x1 << 0)) || !(x & (0x1 << 7))) 
			z = Halt;				
		break; 
		
		
		default:
		break;
	}

	printf("State %x, Input %x, Ouput %x\n", z, x, *y);

}


