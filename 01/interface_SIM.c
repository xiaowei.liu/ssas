
#include <stdio.h>
#include <string.h>
#include <unistd.h>

/* Version INFO 15.11.2021   
Author: LDae
Change: new Module/File due to Redesign

*/

typedef unsigned short int UINT16;
typedef unsigned char UINT8;

void put_output(char* interface, UINT8 y)
{
	if (! strcmp(interface, "Simulation"))
		printf("%x\n", y);
}

void get_input(char *interface, UINT8 *pX)
{
	static int count = 0;
	if (! strcmp(interface, "Test Case 1"))
	{
		if (count == 0)	
			*pX = (0x1 << 0)  | (0x1 << 6) | (0x1 << 7); 
			/* kein Not-AUS, keine Endlage */
		
		if (count == 1)	
			*pX |= (0x1 << 1); /* links gedr�ckt, keine Endlage */
		
		if (count == 2)	
			*pX &= ~(0x1 << 1); /* links nicht mehr gedr�ckt - Taster */
					
		if (count == 5) 
			*pX &= ~(0x1 << 6); /* linke Endlage */

		/* printf("Test Case 1: Step %D, Input %X\n", count, *pX); */
	}
	
	if (! strcmp(interface, "Test Case 2"))
	{
		if (count == 0)	
			*pX = (0x1 << 0)  | (0x1 << 6) | (0x1 << 7); 
			/* kein Not-AUS, keine Endlage */
		
		if (count == 1)	
			*pX |= (0x1 << 2); /* rechts gedr�ckt, keine Endlage */
		
		if (count == 2)	
			*pX &= ~(0x1 << 2); /* rechts nicht mehr gedr�ckt - Taster */
					
		if (count == 5)
			*pX &= ~(0x1 << 7); /* rechte Endlage */
		
		/* printf("Test Case 2: Step %D, Input %X\n", count, *pX); */
	}
	
	if (! strcmp(interface, "Test Case 3"))
	{
		if (count == 0)	
			*pX |= (0x1 << 1)  | (0x1 << 2) | (0x1 << 6) | (0x1 << 7); /* rechts und links gedr�ckt, keine Endlage */
		
		if (count == 1)	{
			*pX &= ~(0x1 << 1); /* links nicht mehr gedr�ckt - Taster */
			*pX &= ~(0x1 << 2); /* rechts nicht mehr gedr�ckt - Taster	*/
		}
		/* printf("Test Case 3: Step %D, Input %X\n", count, *pX); */
	}

	if (! strcmp (interface, "Code Coverage TC1"))
	{
		if (count == 1)
			*pX |= (0x1 << 0)  | (0x1 << 2) | (0x1 << 7);
		
		if (count == 3)
			*pX &= ~(1 << 2); /* right not active anymore */
	}
	count++;	
}